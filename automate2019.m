% Automate modified for 2019 Bundi FLIR ONE RGB images


% Automatically process the images in dir 
% ASA Salgadoe, modified 31/05/2019
% this is extended from automate.m

% calls the function canopyPorosityBatch.m
% automating the thresolding to give porosity values
% iterate all the images in the dir with given threshould, sqC and sqR 
% get the porosity values for each input and write an excel file

% loading the images
disp('start');
startT=fix(clock);
% filePath='C:\Users\asalgado\UNE_Cloud_sync\PhD work\Pilot Test 2_Bundy\Analysis\Matlab\Porosity Work\Batch_images\MobileCam\RGBsmall_selected\Batch1,2&3\';
filePath='C:\Users\asalgado\UNE_Cloud_sync\PhD work\Pilot Test 3_Bundy\Analysis\Mobile_original\img_Trees(noReff)\RGB_shade\ChooseSet2\3. re_selection\';

% srcFiles=dir(strcat(filePath,'IMG_*.JPG'));
srcFiles=dir(strcat(filePath,'*.jpg'));

% Segmentation
% squares=9;
% rows=12;
% cols=5;
fName="";
corrS=0;
corrC=0;
% sq=3;
% thresh=0;
resultA={'Try','Threshould','Sq*Sq','CorrS','CorrC'};
for thresh=0.9
    
   for  sqR=11
%  sqR=12;
%  sqC=5;%:100

imageArray=0;
nameArray={};

% imageArray={'rows x col',rows,cols,'';'Thresh',thresh,'','';'file','Porosity','SimsonR','CiberR'};


for i=1 :length(srcFiles)
    filename=strcat(filePath,srcFiles(i).name);

imName=srcFiles(i).name;

% % % % % % % % % % % % % % % % % % % ********************
% nameArray{i,1}=filename;
% % % % % % % % % % % % % % % % % % 

% 2019 Bundi image nomenclature
imgNum=str2double(extractBefore(imName,'_['));
extractNum= extractBetween(imName,'[',']');
disp(imName);
extractChar=char(extractNum);
ciberR=extractChar;

extractID=extractBetween(imName,']_','.jpg');
treeID=char(extractID);

% For Christina iPhone
% % % imgNum=str2double(extractBefore(imName,'_['));
% % imgNum= str2double(extractBetween(imName,'IMG_','.jpg'));
% % 
% % disp(imgNum);
% % % extractChar=char(extractNum);
% % ciberR=0;
% % 
% % % extractID=extractBetween(imName,']_','.jpg');
% % treeID=0;
% % 

% -----------------------------------


% imageNum
imageArray(i,1)=imgNum;
% Image ID
imageArray(i,2)=str2double(treeID);
% Simson ranking
% Sometimes Simpson Ranking is not Present
%  imageArray(i,2)=str2num(simsonR);

% Ciber ranking**************************************(change)
 imageArray(i,3)=str2num(ciberR);


sqC=sqR-7;
% sqC=sqR-7;
% get porosity values
% [results_cp,results_fF,results_fC,results_clump,results_lai]
[a,b,c,d,e]=canopyPorosityBatch(filename,sqR,sqC,thresh,filePath,imName);

imageArray(i,4)=a;% canopyPorosity


% % imageArray(i,5)=b;%fF
% imageArray(i,5)=c;%fC
% imageArray(i,6)=d;%clump
% imageArray(i,5)=e;%lai


end
fName=sqR+"x"+sqC+"-"+thresh+" CanopyPorosity";
% corelation simpsons ranking with porosity
% corelation ciber ranking wit porosity
% corrS=corr(imageArray(:,1),imageArray(:,3));
% corrC=corr(imageArray(:,2),imageArray(:,3));

% Writing output of Canopy porosity
% imageArray :: cols(1) ImgID, (2)CibaRanking (3)Canopy Porosity
% *************************
% xlswrite(strcat(filePath,fName,'.xlsx'),imageArray);
% *****************
% xlswrite(strcat('C:\Users\asalgado\UNE_Cloud_sync\PhD work\Pilot Test 2_Bundy\Analysis\Matlab\Porosity Work\Batch_images\MobileCam\RGBsmall_selected\Batch1,2&3\',fName,'.xlsx'),imageArray);

% size=0;
% size=length(resultA)+1;

% resultA{size,1}=size;
% resultA{size,2}=thresh;
% resultA{size,3}=sqR;
% resultA{size,4}=corrS;
% resultA{size,5}=corrC;

    end
    
    

end

% xlswrite(strcat(filePath,'Results.xlsx'),resultA);
% %    xlswrite('C:\Users\asalgado\UNE_Cloud_sync\PhD work\Pilot Test 2_Bundy\Analysis\Matlab\Porosity Work\Batch_images\MobileCam\RGBsmall_selected\Batch1,2&3\Results.xlsx',resultA);


disp('End');


