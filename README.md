# CanopyPorosity

Calculate canopy porosity from RGB images. 

Use the MATLAB script `automate2019.m` for bulk processing RGB images. 
It will require the `canopyPorosityBatch.m` script 